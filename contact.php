<?php include 'header.php'; ?>

	<article class="content">

		<div class="page-banner">
			<div class="page-banner-headline">
				<h3 class="feature">CONTACT US</h3>
				<p class="secondary-headline">OR COME VISIT THE SPA RITE SHOWROOM</p>
			</div>	
		</div>

		<div class="wrapper">
			<div class="contact-details-container">
				<div class="contact-details">
					<h3>VISIT OUR SHOWROOM</h3>
					<p>SPA-RITE<br>
					Shop 3, Cnr Bayfield Rd East and Canterbury Rd,<br>
					Bayswater North VIC 3153</p>

					<h3>HOURS</h3>
					<p>MON - SAT: 9am - 5pm<br>
					SUNDAY:   10am - 5pm</p>

					<h3>CONTACT</h3>
					<p>PHONE: (03) 9720 4461<br>
					EMAIL:  danielle@sparite.com.au<br>
					<a href="https://www.facebook.com/sparitespas/"><i class="fa fa-facebook-official" aria-hidden="true"></i> facebook</a></p>
				</div>


				<div class="contact-form">
					<h3>SEND US AN ENQUIRY</h3>
						<form class="contact">
							 <input type="text" placeholder="YOUR NAME"><br>
							 <input type="text" placeholder="YOUR EMAIL"><br>
							 <input type="text" placeholder="SUBJECT"><br>
							 <textarea placeholder="MESSAGE" rows="7"></textarea>
							 <input type="submit" class="button-form" value="SEND ENQUIRY">

						</form>
				</div>
			</div>
		</div>

		<div class="highlight-section">
			<div class="wrapper">
				<h2>WANT TO TRY OUT A SPA BEFORE YOU BUY IT? NOT A PROBLEM – BRING YOUR BATHERS</h2>
				<p class="secondary-headline">WE’VE EVEN GOT A WARDROBE OF SPARES IF YOU FORGET!</p>
				<div class="button"><a href="https://www.google.ca/maps/dir//Spa-Rite,+3%2F200+Canterbury+Rd,+Bayswater+North+VIC+3153,+Australia/@-37.827209,145.280107,17z/data=!4m15!1m6!3m5!1s0x6ad63b1d5c5ea37b:0x8b29652bf4103143!2sSpa-Rite!8m2!3d-37.827209!4d145.282301!4m7!1m0!1m5!1m1!1s0x6ad63b1d5c5ea37b:0x8b29652bf4103143!2m2!1d145.282301!2d-37.827209?hl=en" class="button-link hvr-sweep-to-right">GET DIRECTIONS</a></div>
			</div>
		</div>

	</article>


<?php include 'footer.php'; ?>