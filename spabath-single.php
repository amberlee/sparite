<?php include 'header.php'; ?>

	<article class="content">
		<div class="page-banner">
			<div class="page-banner-headline">
				<h3 class="feature">CEZANNE BATH</h3>
				<p class="secondary-headline">PRODUCTS  <i class="fa fa-long-arrow-right" aria-hidden="true"></i>  SPAS <i class="fa fa-long-arrow-right" aria-hidden="true"></i>  CEZANNE BATH</p>
			</div>	
		</div>

		<div class="wrapper">
			<div class="product-intro">
				<div class="slideshow">
					<div class="arrows">
						<div class="left-arrow">
							<i class="fa fa-angle-left" aria-hidden="true"></i>
						</div>
						<div class="right-arrow">
							<i class="fa fa-angle-right" aria-hidden="true"></i>
						</div>
					</div>
					<img src="assets/images/slideshow-1.jpg" alt="slideshow image">
				</div>
				<div class="description">
					<h5>MANUFACTURING MATERIALS</h5>
					<ul>
						<li><p>Fully moulded acrylic unit with smooth high gloss finish and fibreglass backing for superior strength.</p></li>
					</ul>

					<h5>WARRANTY</h5>
					<p>Cezanne spa bath comes with 5 year warranty.</p>

					<h5>PRICING &amp; ADD ONS</h5>


				<ul class="tab-navigation-horizontal" >
					<li class="active" rel="h-tab1">SPA BATH<br><span class="sml">(No Jets)</span></li>
					<li rel="h-tab2">SPA BATH<br><span class="sml">(With Jets)</span></li>
				  	<li rel="h-tab3">SPA BATH<br><span class="sml">(Upgraded Jets)</span></li>
				  	<li rel="h-tab4">AIRBLOWER<br><span class="sml">(Opt. Upgrade)</span></li>
				</ul>

				<div class="tab_container horizontal">
					<h5 class="d_active tab_drawer_heading-horizontal" rel="h-tab1">SPA BATH (NO JETS) </h5>
				  	<div id="h-tab1" class="tab_content-horizontal">
				  		<p>Sizes Available:</p>
				  	</div>
					  <!-- #tab1 -->
					<h5 class="tab_drawer_heading-horizontal" rel="h-tab2">SPA BATH (WITH JETS) </h5>
				 	<div id="h-tab2" class="tab_content-horizontal">
				  		<p>Sizes Available:</p>
				  	</div>
					  <!-- #tab2 -->
					<h5 class="tab_drawer_heading-horizontal" rel="h-tab3">SPA BATH (UPGRADED JETS) </h5>
					<div id="h-tab3" class="tab_content-horizontal">
					 	<p>Sizes Available:</p>
					</div>
					  <!-- #tab3 -->
					<h5 class="tab_drawer_heading-horizontal" rel="h-tab4">AIRBLOWER (OPT. UPGRADE) </h5>
					<div id="h-tab4" class="tab_content-horizontal">
						<p>Sizes Available:</p>
					</div>
					  <!-- #tab4 --> 
				</div>

				<div class="button-pink"><a href="#" class="button-pink-link">SEND ENQUIRY</a></div>

				</div>
			</div>


			<ul class="tab-navigation">
				<li class="active" rel="tab1">ABOUT</li>
				<li rel="tab2">SPEC SHEET</li>
			  	<li rel="tab3">COLOURS</li>
			  	<li rel="tab4">FEATURES</li>
			</ul>

			<div class="tab_container">
				<p class="d_active tab_drawer_heading" rel="tab1">ABOUT </p>
			  	<div id="tab1" class="tab_content">
			  		<h2>ABOUT CEZANNE BATH</h2>
			  	</div>
				  <!-- #tab1 -->
				<p class="tab_drawer_heading" rel="tab2">SPEC SHEET </p>
			 	<div id="tab2" class="tab_content">
			  		<h2>SPEC SHEET</h2>
			  	</div>
				  <!-- #tab2 -->
				<p class="tab_drawer_heading" rel="tab3">COLOURS </p>
				<div id="tab3" class="tab_content">
				 	<h2>COLOURS</h2>
				</div>
				  <!-- #tab3 -->
				<p class="tab_drawer_heading" rel="tab4">FEATURES </p>
				<div id="tab4" class="tab_content">
					<h2>FEATURES</h2>
				</div>
				  <!-- #tab4 --> 
			</div>


		</div>


		<div class="highlight-section">
			<div class="wrapper">
				<h2>BOOK A WET TEST</h2>
				<p class="secondary-headline">TRY THE SPA BEFORE YOU BUY</p>
				<form class="wet-test">
					 <input type="text" placeholder="FIRST NAME"><br>
					 <input type="text" placeholder="LAST NAME"><br>
					 <input type="text" placeholder="PHONE NUMBER"><br>
					 <input type="text" placeholder="EMAIL"><br>
					 <div class="button"><a href="#" class="button-link hvr-sweep-to-right">VIEW BUYERS GUIDE</a></div>
				</form>
			</div>
		</div>


	</article>

<?php include 'footer.php'; ?>
