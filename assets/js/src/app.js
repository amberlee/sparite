$(document).ready(function(){

	//Hover Main Nav
	$(".main-nav li.menu-item-has-children").hover(function(e){

		var child = $(this).find("ul.sub-menu");
		$(child).addClass("sub-menu-active");

	}, function(e){

		var child = $(this).find("ul.sub-menu");
		$(child).removeClass("sub-menu-active");

	});

	//Mobile Toggle
	$("#mobile-nav-toggle").on("click", function(e){
		e.preventDefault();
		$("#mobile-nav").toggleClass("mobile-nav-active");
	});

	//Hover Main Nav
	$(".mobile-nav li.menu-item-has-children").on("click", function(e){

		var child = $(this).find("ul.sub-menu");
		$(child).toggleClass("sub-menu-active");
	});
	
	//Accordian

	$('.faq-content').hide();
		$('.faq-title').on('click', function() {
		$(this).find('.arrow-left').toggleClass('arrow-up');
		$(this).next().slideToggle('700');
	});

	//Mobile Filter

		$('.filter-title').on('click', function() {
		$(this).find('.arrow-left').toggleClass('arrow-up');
		$(this).next().slideToggle('700');
	});

	//Vertical Align Overlay
	var siblingheight = $('.overlay').next('.post-name, .product-name').height();

	$('.overlay p').css({ "padding-bottom": siblingheight + "px" });

	//Horizontal and Vertical Tabs to Mobile Accordian

	$(".tab_content").hide();
    $(".tab_content:first").show();

  /* if in tab mode */
    $("ul.tab-navigation li").click(function() {
		
      $(".tab_content").hide();
      var activeTab = $(this).attr("rel"); 
      $("#"+activeTab).fadeIn();		
		
      $("ul.tab-navigation li").removeClass("active");
      $(this).addClass("active");

	  $(".tab_drawer_heading").removeClass("d_active");
	  $(".tab_drawer_heading[rel^='"+activeTab+"']").addClass("d_active");
	  
    });
	/* if in drawer mode */
	$(".tab_drawer_heading").click(function() {
      
      $(".tab_content").hide();
      var d_activeTab = $(this).attr("rel"); 
      $("#"+d_activeTab).fadeIn();
	  
	  $(".tab_drawer_heading").removeClass("d_active");
      $(this).addClass("d_active");
	  
	  $("ul.tab-navigation li").removeClass("active");
	  $("ul.tab-navigation li[rel^='"+d_activeTab+"']").addClass("active");
    });
	
	
	/* Extra class "tab_last" 
	   to add border to right side
	   of last tab */
	$('ul.tab-navigation li').last().addClass("tab_last");


	$(".tab_content-horizontal").hide();
    $(".tab_content-horizontal:first").show();

  /* if in tab mode */
    $("ul.tab-navigation-horizontal li").click(function() {
		
      $(".tab_content-horizontal").hide();
      var activeTab = $(this).attr("rel"); 
      $("#"+activeTab).fadeIn();		
		
      $("ul.tab-navigation-horizontal li").removeClass("active");
      $(this).addClass("active");

	  $(".tab_drawer_heading-horizontal").removeClass("d_active");
	  $(".tab_drawer_heading-horizontal[rel^='"+activeTab+"']").addClass("d_active");
	  
    });
	/* if in drawer mode */
	$(".tab_drawer_heading-horizontal").click(function() {
      
      $(".tab_content-horizontal").hide();
      var d_activeTab = $(this).attr("rel"); 
      $("#"+d_activeTab).fadeIn();
	  
	  $(".tab_drawer_heading-horizontal").removeClass("d_active");
      $(this).addClass("d_active");
	  
	  $("ul.tab-navigation-horizontal li").removeClass("active");
	  $("ul.tab-navigation-horizontal li[rel^='"+d_activeTab+"']").addClass("active");
    });
	
	
	/* Extra class "tab_last" 
	   to add border to right side
	   of last tab */
	$('ul.tab-navigation-horizontal li').last().addClass("tab_last");
	

});
