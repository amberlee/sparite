<?php include 'header.php'; ?>

	<article class="content">

		<div class="page-banner">
			<div class="page-banner-headline">
				<h3 class="feature">PRODUCTS</h3>
				<p class="secondary-headline">CLICK TO VIEW MORE OF OUR EXTENSIVE PRODUCT RANGE</p>
			</div>	
		</div>

		<div class="wrapper">
			<div class="product-box-container col-4">

				<a href="productrange.php" class="product-box">
					<div class="overlay">
						<p>VIEW RANGE</p>
					</div>
					<div class="product-name">
						<h3>SPAS</h3>
					</div>
					<img src="assets/images/product-image.jpg"/>
				</a>

				<a href="productrange.php" class="product-box">
					<div class="overlay">
						<p>VIEW RANGE</p>
					</div>
					<div class="product-name">
						<h3>SWIM SPAS</h3>
					</div>
					<img src="assets/images/product-image.jpg"/>
				</a>

				<a href="productrange.php" class="product-box">
					<div class="overlay">
						<p>VIEW RANGE</p>
					</div>
					<div class="product-name">
						<h3>IN GROUND SPAS</h3>
					</div>
					<img src="assets/images/product-image.jpg"/>
				</a>

				<a href="productrange.php" class="product-box">
					<div class="overlay">
						<p>VIEW RANGE</p>
					</div>
					<div class="product-name">
						<h3>SPA BATHS</h3>
					</div>
					<img src="assets/images/product-image.jpg"/>
				</a>
	
				<a href="productrange.php" class="product-box">
					<div class="overlay">
						<p>VIEW RANGE</p>
					</div>
					<div class="product-name">
						<h3>ACCESSORIES</h3>
					</div>
					<img src="assets/images/product-image.jpg"/>
				</a>

				<a href="productrange.php" class="product-box">
					<div class="overlay">
						<p>VIEW RANGE</p>
					</div>
					<div class="product-name">
						<h3>GAZEBOS</h3>
					</div>
					<img src="assets/images/product-image.jpg"/>
				</a>

				<a href="productrange.php" class="product-box">
					<div class="overlay">
						<p>VIEW RANGE</p>
					</div>
					<div class="product-name">
						<h3>EQUIPMENT</h3>
					</div>
					<img src="assets/images/product-image.jpg"/>
				</a>
	
				<a href="productrange.php" class="product-box">
					<div class="overlay">
						<p>VIEW RANGE</p>
					</div>
					<div class="product-name">
						<h3>SPECIALS</h3>
					</div>
					<img src="assets/images/product-image.jpg"/>
				</a>
				<a class="product-box" id="empty"></a>
				<a class="product-box" id="empty"></a>

			</div>
		</div>


		<div class="highlight-section" id="sml-margin">
			<div class="wrapper">
				<h2>NEED HELP FINDING A SPA?</h2>
				<p class="secondary-headline">VIEW OUR GUIDE TO KNOW WHAT THINGS TO LOOK FOR WHEN FINDING A SPA</p>
				<div class="button"><a href="#" class="button-link hvr-sweep-to-right">VIEW BUYERS GUIDE</a></div>
			</div>
		</div>

	</article>

	
<?php include 'footer.php'; ?>