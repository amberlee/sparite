<?php include 'header.php'; ?>

	<article class="content">

		<div class="page-banner">
			<div class="page-banner-headline">
				<h3 class="feature">SPAS</h3>
				<p class="secondary-headline">PRODUCTS  <i class="fa fa-long-arrow-right" aria-hidden="true"></i>  SPAS</p>
			</div>	
		</div>

		<div class="wrapper product-list">
			<div class="filter-title tablet-hidden">
				<p>FILTER PRODUCTS <span class="arrow-left"></span></p>
			</div>
			<div class="sidebar">
				<h3>FILTERS </h3>
				<form>
				<label><p>PRODUCT TYPES</p></label>
				<select name="product-type">
					<option value="spas">SPAS</option>
					<option value="swim-spas">SWIM SPAS</option>
					<option value="inground-spas">IN GROUND SPAS</option>
					<option value="spa-baths">SPA BATHS</option>
					<option value="accessories">ACCESSORIES</option>
					<option value="gazebos">GAZEBOS</option>
					<option value="equipment">EQUIPMENT</option>
					<option value="specials">SPECIALS</option>
					<option value="specials">SPARE PARTS</option>
				</select>
				<label><p>BRANDS</p></label>
				<p class="grey-title">FOR SPAS:</p>
				<input type="checkbox" name="brands" value="Entertainer" class="checkbox"><p class="checkbox-label">ENTERTAINER</p><br>
  				<input type="checkbox" name="brands" value="XL" class="checkbox"><p class="checkbox-label">XL</p><br>
  				<input type="checkbox" name="brands" value="Platinum" class="checkbox"><p class="checkbox-label">PLATINUM</p><br>
  				<p class="grey-title">FOR SWIM SPAS:</p>
				<input type="checkbox" name="brands" value="Day Dream" class="checkbox"><p class="checkbox-label">DAY DREAM</p><br>
  				<input type="checkbox" name="brands" value="Platinum" class="checkbox"><p class="checkbox-label">PLATINUM</p><br>

  				<label><p>PRICE</p></label>
  					<div class="slider">
		  				<p>$0</p>
		  				<p class="right">$40,000</p>
		  				<input type="range" name="price" value="20000" min="0" max="40000">
		  			</div>

  				<label><p>POWER USEAGE</p></label>
  					<div class="slider">
		  				<p>LOW</p>
		  				<p class="right">HIGH</p>
		  				<input type="range" name="power-useage" value="20000" min="0" max="40000">
		  			</div>

  				<label><p>NUMBER OF PEOPLE</p></label>
  					<div class="slider">
		  				<p>1</p>
		  				<p class="right">12</p>
		  				<input type="range" name="people" value="20000" min="0" max="40000">
		  			</div>
				

				</form>

			</div>

			<div class="products">
				<div class="product-box-container col-3">

					<a href="product-single.php" class="product-box">
						<div class="sale">
							<p>SALE</p>
						</div>
						<div class="overlay">
							<p>VIEW</p>
						</div>
						<div class="product-name">
							<h3>SPAS</h3>
							<p class="prouduct-details"><i class="fa fa-user" aria-hidden="true"> </i> 5
							<span class="right"><img src="assets/images/ruler-icon.png" alt="Ruler Icon"> 2m x 2m x 820mm</span></p>
						</div>
						<img src="assets/images/product-image.jpg"/>
					</a>

					<a href="product-single.php" class="product-box">
						<div class="overlay">
							<p>VIEW</p>
						</div>
						<div class="product-name">
							<h3>SWIM SPAS</h3>
							<p class="prouduct-details"><i class="fa fa-user" aria-hidden="true"> </i> 5
							<span class="right"><img src="assets/images/ruler-icon.png" alt="Ruler Icon"> 2m x 2m x 820mm</span></p>
						</div>
						<img src="assets/images/product-image.jpg"/>
					</a>

					<a href="product-single.php" class="product-box">
						<div class="sale">
							<p>SALE</p>
						</div>
						<div class="overlay">
							<p>VIEW</p>
						</div>
						<div class="product-name">
							<h3>IN GROUND SPAS</h3>
							<p class="prouduct-details"><i class="fa fa-user" aria-hidden="true"> </i> 5
							<span class="right"><img src="assets/images/ruler-icon.png" alt="Ruler Icon"> 2m x 2m x 820mm</span></p>
						</div>
						<img src="assets/images/product-image.jpg"/>
					</a>

					<a href="product-single.php" class="product-box">
						<div class="overlay">
							<p>VIEW</p>
						</div>
						<div class="product-name">
							<h3>SPA BATHS</h3>
						    <p class="prouduct-details"><i class="fa fa-user" aria-hidden="true"> </i> 5
							<span class="right"><img src="assets/images/ruler-icon.png" alt="Ruler Icon"> 2m x 2m x 820mm</span></p>
						</div>
						<img src="assets/images/product-image.jpg"/>
					</a>

					<a href="product-single.php" class="product-box">
						<div class="sale">
							<p>SALE</p>
						</div>
						<div class="overlay">
							<p>VIEW</p>
						</div>
						<div class="product-name">
							<h3>ACCESSORIES</h3>
							<p class="prouduct-details"><i class="fa fa-user" aria-hidden="true"> </i> 5
							<span class="right"><img src="assets/images/ruler-icon.png" alt="Ruler Icon"> 2m x 2m x 820mm</span></p>
						</div>
						<img src="assets/images/product-image.jpg"/>
					</a>

					<a href="product-single.php" class="product-box">
						<div class="overlay">
							<p>VIEW</p>
						</div>
						<div class="product-name">
							<h3>GAZEBOS</h3>
							<p class="prouduct-details"><i class="fa fa-user" aria-hidden="true"> </i> 5
							<span class="right"><img src="assets/images/ruler-icon.png" alt="Ruler Icon"> 2m x 2m x 820mm</span></p>
						</div>
						<img src="assets/images/product-image.jpg"/>
					</a>

					<a href="product-single.php" class="product-box">
						<div class="sale">
							<p>SALE</p>
						</div>
						<div class="overlay">
							<p>VIEW</p>
						</div>
						<div class="product-name">
							<h3>EQUIPMENT</h3>
							<p class="prouduct-details"><i class="fa fa-user" aria-hidden="true"> </i> 5
							<span class="right"><img src="assets/images/ruler-icon.png" alt="Ruler Icon"> 2m x 2m x 820mm</span></p>
						</div>
						<img src="assets/images/product-image.jpg"/>
					</a>

					<a href="product-single.php" class="product-box">
						<div class="overlay">
							<p>VIEW</p>
						</div>
						<div class="product-name">
							<h3>SPECIALS</h3>
							<p class="prouduct-details"><i class="fa fa-user" aria-hidden="true"> </i> 5
							<span class="right"><img src="assets/images/ruler-icon.png" alt="Ruler Icon"> 2m x 2m x 820mm</span></p>
						</div>
						<img src="assets/images/product-image.jpg"/>
					</a>

					<a class="product-box" id="empty"></a>
					<a class="product-box" id="empty"></a>
				</div>
			</div>
		</div>


		<div class="highlight-section" id="sml-margin">
			<div class="wrapper">
				<h2>NEED HELP FINDING A SPA?</h2>
				<p class="secondary-headline">VIEW OUR GUIDE TO KNOW WHAT THINGS TO LOOK FOR WHEN FINDING A SPA</p>
				<div class="button"><a href="#" class="button-link hvr-sweep-to-right">VIEW BUYERS GUIDE</a></div>
			</div>
		</div>

	</article>


<?php include 'footer.php'; ?>