<?php include 'header.php'; ?>
		
	<article class="content">

		<div class="page-banner">
			<div class="page-banner-headline">
				<h3 class="feature">OWNER MANUALS</h3>
				<p class="secondary-headline">DOWNLOAD A PDF OF OUR SPAS OWNER MANUAL</p>
			</div>	
		</div>

		<div class="wrapper">

			<div class="accordian">

				<div class="faq-title">
					<div class="arrow-left"></div>
					<p>Oasis Spas Owners Manual</p>
				</div>
				<div class="faq-content">
					<div class="col-4">
						<a href="#"><i class="fa fa-file-pdf-o" aria-hidden="true"></i>
						<p>Spa Owners Manual<br><span class="sml">DOWNLOAD</span></p></a>
					</div>
				</div>

				<div class="faq-title">
					<div class="arrow-left"></div>
					<p>Spa Net XS Control System Manual </p>
				</div>
				<div class="faq-content">
					<div class="col-4">
						<a href="#"><i class="fa fa-file-pdf-o" aria-hidden="true"></i>
						<p>Spa Net XS Control System Manual<br><span class="sml">DOWNLOAD</span></p></a>
					</div>
				</div>

				<div class="faq-title">
					<div class="arrow-left"></div>
					<p>Spa Net SV Control System Manual </p>
				</div>
				<div class="faq-content">
					<div class="col-4">
						<a href="#"><i class="fa fa-file-pdf-o" aria-hidden="true"></i>
						<p>Spa Net SV User Manual<br><span class="sml">DOWNLOAD</span></p></a>
					</div>
					<div class="col-4">
						<a href="#"><i class="fa fa-file-pdf-o" aria-hidden="true"></i>
						<p>SV2 Quick Start Guide<br><span class="sml">DOWNLOAD</span></p></a>
					</div>
					<div class="col-4">
						<a href="#"><i class="fa fa-file-pdf-o" aria-hidden="true"></i>
						<p>SV3 Quick Start Guide<br><span class="sml">DOWNLOAD</span></p></a>
					</div>
					<div class="col-4">
						<a href="#"><i class="fa fa-file-pdf-o" aria-hidden="true"></i>
						<p>SV4 Quick Start Guide<br><span class="sml">DOWNLOAD</span></p></a>
					</div>
					<div class="col-4">
						<a href="#"><i class="fa fa-file-pdf-o" aria-hidden="true"></i>
						<p>SV2 Troubleshooting Guide<br><span class="sml">DOWNLOAD</span></p></a>
					</div>
				</div>
				<div class="faq-title">
					<div class="arrow-left"></div>
					<p>Coast Spas Owners Manual </p>
				</div>
				<div class="faq-content">
					<div class="col-4">
						<a href="#"><i class="fa fa-file-pdf-o" aria-hidden="true"></i>
						<p>Coast Tublicious Owners Guide<br><span class="sml">DOWNLOAD</span></p></a>
					</div>
					<div class="col-4">
						<a href="#"><i class="fa fa-file-pdf-o" aria-hidden="true"></i>
						<p>Coast Balboa Owners Manual<br><span class="sml">DOWNLOAD</span></p></a>
					</div>
					<div class="col-4">
						<a href="#"><i class="fa fa-file-pdf-o" aria-hidden="true"></i>
						<p>Coast Spa Owners Manual<br><span class="sml">DOWNLOAD</span></p></a>
					</div>
				</div>
				<div class="faq-title">
					<div class="arrow-left"></div>
					<p>Chemical Help Sheet Manuals </p>
				</div>
				<div class="faq-content">
					<div class="col-4">
						<a href="#"><i class="fa fa-file-pdf-o" aria-hidden="true"></i>
						<p>Lithium Chemical User Guide<br><span class="sml">DOWNLOAD</span></p></a>
					</div>
					<div class="col-4">
						<a href="#"><i class="fa fa-file-pdf-o" aria-hidden="true"></i>
						<p>Crystal Waters SPA User Guide<br><span class="sml">DOWNLOAD</span></p></a>
					</div>
					<div class="col-4">
						<a href="#"><i class="fa fa-file-pdf-o" aria-hidden="true"></i>
						<p>Crystal Waters POOL User Guide<br><span class="sml">DOWNLOAD</span></p></a>
					</div>
				</div>
				<div class="faq-title">
					<div class="arrow-left"></div>
					<p>Spa Winterization </p>
				</div>
				<div class="faq-content">
					<div class="col-4">
						<a href="#"><i class="fa fa-file-pdf-o" aria-hidden="true"></i>
						<p>Spa Winterization Guide<br><span class="sml">DOWNLOAD</span></p></a>
					</div>
				</div>

			</div>

		</div>

	</article>

<?php include 'footer.php'; ?>