<?php include 'header.php'; ?>

	<article class="content">
		<div class="page-banner">
			<div class="page-banner-headline">
				<h3 class="feature">NOOSA ENTERTAINER</h3>
				<p class="secondary-headline">PRODUCTS  <i class="fa fa-long-arrow-right" aria-hidden="true"></i>  SPAS <i class="fa fa-long-arrow-right" aria-hidden="true"></i>  NOOSA ENTERTAINER</p>
			</div>	
		</div>

		<div class="wrapper">
			<div class="product-intro">
				<div class="slideshow">
					<div class="arrows">
						<div class="left-arrow">
							<i class="fa fa-angle-left" aria-hidden="true"></i>
						</div>
						<div class="right-arrow">
							<i class="fa fa-angle-right" aria-hidden="true"></i>
						</div>
					</div>
					<img src="assets/images/slideshow-1.jpg" alt="slideshow image">
				</div>
				<div class="description">
				<h5>DESCRIPTION</h5>
				<p>The Noosa is a roomy yet compact 5 person spa with 2 luxury recliner lounges, 2 seats and an entry step/cool off seat. This spa is perfect for families who want massage, but who also have a small budget.</p>

				<h5>INCLUDES</h5>
				<ul>
					<li><p> full body massage lounge</p></li>
					<li><p>3 hydrotherapy seats</p></li>
					<li><p>Compact design with seating for 5</p></li>
				</ul>

				<div class="button-pink"><a href="#openModal" class="button button-pink-link">SEND ENQUIRY</a></div>

				</div>
			</div>

			<ul class="tab-navigation">
				<li class="active" rel="tab1">ABOUT</li>
				<li rel="tab2">SPEC SHEET</li>
			  	<li rel="tab3">COLOURS</li>
			  	<li rel="tab4">FEATURES</li>
			</ul>

			<div class="tab_container">
				<p class="d_active tab_drawer_heading" rel="tab1">ABOUT </p>
			  	<div id="tab1" class="tab_content">
			  		<h2>ABOUT NOOSA ENTERTAINER</h2>
			  		<div class="col-2">
						<img src="assets/images/noosa-seatmap.png" alt="Noosa Seat Map">
					</div>
					<div class="col-2">
						<p><i class="fa fa-user" style="color: #ad5dd4;" aria-hidden="true"></i> Cool off seat: This higher positioned seat is perfect not only for small children to keep their heads above water but is also great for adults for when the spa water is getting too hot but don’t want to leave the spa.</p>

						<p><i class="fa fa-user" style="color: #e4e621;" aria-hidden="true"></i> Cool off seat: This higher positioned seat is perfect not only for small children to keep their heads above water but is also great for adults for when the spa water is getting too hot but don’t want to leave the spa.</p>

						<p><i class="fa fa-user" style="color: #e773be;"aria-hidden="true"></i> Cool off seat: This higher positioned seat is perfect not only for small children to keep their heads above water but is also great for adults for when the spa water is getting too hot but don’t want to leave the spa.</p>

					</div>
					<div class="col-2">
						<img src="assets/images/noosa-measurement.png" alt="Noosa Measurement">
					</div>
					<div class="col-2">
						<img src="assets/images/noosa-top-measurement.png" alt="Noosa Top Measurement">
					</div>
			  	</div>
				  <!-- #tab1 -->
				<p class="tab_drawer_heading" rel="tab2">SPEC SHEET </p>
			 	<div id="tab2" class="tab_content">
			  		<h2>SPEC SHEET</h2>
			  	</div>
				  <!-- #tab2 -->
				<p class="tab_drawer_heading" rel="tab3">COLOURS </p>
				<div id="tab3" class="tab_content">
				 	<h2>COLOURS</h2>
				</div>
				  <!-- #tab3 -->
				<p class="tab_drawer_heading" rel="tab4">FEATURES </p>
				<div id="tab4" class="tab_content">
					<h2>FEATURES</h2>
				</div>
				  <!-- #tab4 --> 
			</div>

		</div>

		<div class="highlight-section">
			<div class="wrapper">
				<h2>BOOK A WET TEST</h2>
				<p class="secondary-headline">TRY THE SPA BEFORE YOU BUY</p>
				<form class="wet-test">
					 <input type="text" placeholder="FIRST NAME"><br>
					 <input type="text" placeholder="LAST NAME"><br>
					 <input type="text" placeholder="PHONE NUMBER"><br>
					 <input type="text" placeholder="EMAIL"><br>
					 <div class="button"><a href="#" class="button-link hvr-sweep-to-right">VIEW BUYERS GUIDE</a></div>
				</form>
			</div>
		</div>

		<div id="openModal" class="modalbg">
			<div class="dialog">
			    <a href="#close" title="Close" class="close"><i class="fa fa-times" aria-hidden="true"></i></a>
			  	<h2>RECEIVE A QUOTE ON YOUR SPA</h2>
					<p>We will send you additional information about your spa regarding availability, colours available and pricing.</p>
					<form class="contact">
					 	<input type="text" placeholder="YOUR NAME"><br>
						<input type="text" placeholder="YOUR EMAIL"><br>
						<input type="text" placeholder="PHONE NUMBER"><br>
						<textarea placeholder="ADDITIONAL COMMENTS" rows="7"></textarea>
						<input type="submit" class="button-form" value="SEND ENQUIRY">
					</form>
			</div>
		</div>
	</article>


<?php include 'footer.php'; ?>