<?php include 'header.php'; ?>
		
	<article class="content">

		<div class="page-banner">
			<div class="page-banner-headline">
				<h3 class="feature">BUILD YOUR OWN SPA</h3>
				<p class="secondary-headline">FILL IN THE FORM TO FIND THE PERFECT SPA</p>
			</div>	
		</div>

		<div class="wrapper">

			<div class="build-spa">
				<div class="filter-title">
					<p>WHAT ARE YOU LOOKING FOR? <span class="arrow-left"></span></p>
				</div>

				<div class="sidebar">
					<h3>WHAT ARE YOU LOOKING FOR?</h3>
					<form>
					<label><p>TYPE OF SPA</p></label>
					<input type="checkbox" name="brands" value="Spa" class="checkbox"><p class="checkbox-label">SPA</p><br>
	  				<input type="checkbox" name="brands" value="Swim Spa" class="checkbox"><p class="checkbox-label">Swim Spa</p><br>

					<label><p>SIZE REQUIREMENT</p></label>
					<select name="size-requirement">
						<option value="size-1">1.6m x 2m</option>
						<option value="size-2">2m x 2m and under</option>
						<option value="size-3">2.4 x 2.3 and under</option>
						<option value="size-4">Bigger than 2.4 x 2.3</option>
						<option value="size-5">Swim Spa</option>
					</select>

					<label><p>SEATING</p></label>
					<select name="size-requirement">
						<option value="seat-1">3-4 Seater</option>
						<option value="seat-2">5-6 Seater</option>
						<option value="seat-3">7-8 Seater</option>
						<option value="seat-4">8-10 Seater</option>
					</select>

					<label><p>PURPOSE OF SPA</p></label>
					<select name="purpose">
						<option value="Social">Social</option>
						<option value="Hydrotherapy">Hydrotherapy</option>
						<option value="Both">Both</option>
					</select>

					<label><p>LEVEL OF MASSAGE</p></label>
					<div class="slider">
		  				<p>1</p>
		  				<p class="right">10</p>
		  				<input type="range" name="price" value="5" min="0" max="10">
		  			</div>

		  			<label><p>IS THE DAILY RUNNING COST OF YOUR SPA IMPORTANT TO YOU?</p></label>
					<input type="radio" class="radio" name="cost" value="yes"><p class="radio-text"> Yes</p><br>
					<input type="radio" class="radio" name="cost" value="no"><p class="radio-text"> No</p><br>

					<label><p>RUNNING COST</p></label>
					<select name="cost">
						<option value="cost-1">4 levels of insulation (Approx. $1.50/day per 1,000L)</option>
						<option value="cost-2">6 levels of insulation (Approx. $1.30/day per 1,000L)</option>
						<option value="cost-3">8 levels of insulation (Approx. $1.08/day per 1,000L)</option>
					</select>

					<label><p class="has-tooltip">CONTROLLER PREFERENCE</p></label>
					<div class="tooltip">?
						<div class="tooltip-content">
							<p>Spa Tech and Spa Net Description</p>
						</div>
					</div>
					<select name="controller">
						<option value="spa-tech">Spa Tech</option>
						<option value="spa-net">Spa Net Smart Controller</option>
					</select>


					<label><p class="has-tooltip">WOULD YOU LIKE OZONE ON YOUR SPA?</p></label>
					<div class="tooltip">?
						<div class="tooltip-content">
							<p>Ozone cuts back chemical usage on spa and helps kill the bacteria before it gets into your spa.</p>
						</div>
					</div>
					<div>
						<input type="radio" class="radio" name="ozone" value="yes"><p class="radio-text"> Yes</p><br>
						<input type="radio" class="radio" name="ozone" value="no"><p class="radio-text"> No</p><br>
					</div>

					<label><p class="has-tooltip">SPA TO HAVE HEAT PUMP/GAS HEATER CONNECTIONS?</p></label>
					<div class="tooltip">?
						<div class="tooltip-content">
							<p>Heat Pumps save up to 75% on your spas running cost. Recommended for swim spas</p>
						</div>
					</div>
					<div>
						<input type="radio" class="radio" name="heat" value="yes"><p class="radio-text"> Yes</p><br>
						<input type="radio" class="radio" name="heat" value="no"><p class="radio-text"> No</p><br>
					</div>
					

					<label><p>BUDGET (EXCLUDING INSTALLATION AND DELIVERY)</p></label>
					<div class="slider">
		  				<p>$0</p>
		  				<p class="right">$40,000</p>
		  				<input type="range" name="price" value="5" min="0" max="40,000">
		  			</div>

		  			 <input type="submit" class="button-form" value="BUILD YOUR SPA">

					</form>

				</div>

				<div class="results">
					<div class="heading">
						<div class="text">
							<h2>YOUR SPA DESIGN</h2>
							<p>Based on your requirements we currently recommend the following:</p>
						</div>

						<div class="button-pink"><a href="#openModal" class="button button-pink-link">NEXT</a></div>

					</div>

					<div class="slideshow">
						<div class="arrows">
							<div class="left-arrow">
								<i class="fa fa-angle-left" aria-hidden="true"></i>
							</div>
							<div class="right-arrow">
								<i class="fa fa-angle-right" aria-hidden="true"></i>
							</div>
						</div>
						<img src="assets/images/slideshow-1.jpg" alt="slideshow image">
					</div>

					<div class="description">
						<h5>NOOSA ENTERTAINER</h5>
						<p>The Noosa is a roomy yet compact 5 person spa with 2 luxury recliner lounges, 2 seats and an entry step/cool off seat. This spa is perfect for families who want massage, but who also have a small budget.</p>
						<div class="button-pink"><a href="#openModal" class="button button-pink-link">NEXT</a></div>
					</div>

					<h3> ALL RESULTS:</h3>

					<div class="product-box-container col-2">

						<div tabindex="-1" class="product-box">
							<div class="overlay">
								<p>SELECTED</p>
							</div>
							<div tabindex="-1" class="product-name">
								<h3>NOOSA ENTERTAINER</h3>
							</div>
							<img src="assets/images/product-image.jpg"/>
						</div>

						<div tabindex="-1" class="product-box">
							<div class="overlay">
								<p>SELECTED</p>
							</div>
							<div tabindex="-1" class="product-name">
								<h3>WHITEHAVEN PLATINUM</h3>
							</div>
							<img src="assets/images/product-image.jpg"/>
						</div>

						<div tabindex="-1" class="product-box">
							<div class="overlay">
								<p>SELECTED</p>
							</div>
							<div class="product-name">
								<h3>RIVERIA PLATINUM</h3>
							</div>
							<img src="assets/images/product-image.jpg"/>
						</div>
					</div>
				</div>

			</div>
		</div>
		<div id="openModal" class="modalbg">
			<div class="dialog">
			    <a href="#close" title="Close" class="close"><i class="fa fa-times" aria-hidden="true"></i></a>
			  	<h2>RECEIVE A QUOTE ON YOUR SPA</h2>
					<p>We will send you additional information about your spa regarding availability, colours available and pricing.</p>
					<form class="contact">
					 	<input type="text" placeholder="YOUR NAME"><br>
						<input type="text" placeholder="YOUR EMAIL"><br>
						<input type="text" placeholder="PHONE NUMBER"><br>
						<textarea placeholder="ADDITIONAL COMMENTS" rows="7"></textarea>
						<input type="submit" class="button-form" value="SEND ENQUIRY">
					</form>
			</div>
		</div>

	</article>


<?php include 'footer.php'; ?>