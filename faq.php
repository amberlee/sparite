<?php include 'header.php'; ?>

	<article class="content">

		<div class="page-banner">
			<div class="page-banner-headline">
				<h3 class="feature">FREQUENTLY ASKED QUESTIONS</h3>
				<p class="secondary-headline">IF YOUR QUESTION IS NOT ANSWERED HERE PLEASE CONTACT US</p>
			</div>	
		</div>

		<div class="wrapper">

			<div class="accordian">

				<div class="faq-title">
					<div class="arrow-left"></div>
					<p>WHAT THINGS SHOULD I CONSIDER BEFORE PURCHASING A SPA?</p>
				</div>
				<div class="faq-content">
					<p>At Spa-Rite Spas we stock a range of spa brands, sizes and layouts. It is important to think when considering your purchase of a spa what is important to you. At Spa-Rite we break this down into a number of categories. Size and criteria are the upmost important. Do not buy a spa that doesn’t fit the space and don’t buy a spa that is bigger than what you need. Remember the larger the spa, the higher the running cost.</p>

					<p>Other questions you should think about are:</p>

					<p><strong>Why would you like a spa? For relaxation and hydrotherapy, for entertaining or for a combination of the two?</strong></p>

					<p>Are running costs important? Our Coast range are fully insulated, meaning you have the lowest running cost available. Whilst our Oasis range can have up to 8 levels of insulation (available in selected models) which trap in hot air, reducing your spas running cost.</p>

					<p><strong>How much power do you have available at your fuse box?</strong></p>

					<p>Do you require a hardwired spa or a plug in spa? – Remember a 15amp spa still requires a qualified electrician. If considering buying a load shed spa, remember that only the blower, heater or jets can work only one at a time. In other words when the pump for the jets is on full power the heater will automatically turn off.</p>

					<p>At Spa-Rite choosing what spa best suits you is made easier. Due to our Try Before you Buy Program, simply just book an appointment and try out our spas, to assist you with what spa most suits your needs.</p>
				</div>

				<div class="faq-title">
					<div class="arrow-left"></div>
					<p>Do I need to install any Plumbing with a portable spa?</p>
				</div>
				<div class="faq-content">
					<p>Plumbers are only needed if you are planning to run an external heating source. All of our portable spas already have the plumbing connections underneath the cabinet.</p>

					<p>However if you buy a Heat Pump you will need both a qualified electrician and plumber to install your Heat Pump.</p>
				</div>

				<div class="faq-title">
					<div class="arrow-left"></div>
					<p>How much money will a Heat Pump save me?</p>
				</div>
				<div class="faq-content">
					<p>At Spa-Rite we have conducted testing on an Oasis Brand 5,500 litre swim spa in Melbourne during winter.</p>

					<p>With the Energy Smart Plus insulation system fitted (which locks heat into the swim spa cabinet), the weekly costs to heat this amount of water to 32 degrees averaged only $9.87 per week. This would save you over 75% of the cost of heating a similar swim spa with normal insulation and an electric heater element.</p>
				</div>

				<div class="faq-title">
					<div class="arrow-left"></div>
					<p>How often do I need to clean the Water?</p>
				</div>
				<div class="faq-content">
					<p>It is important to dose your spa as per your chemical instructions to maintain healthy and balanced water. At Spa-Rite we guarantee to help you with all your chemical questions and advise you on what chemical system best suits your needs. Dosing your spa is quick with both of our recommended chemical systems: The Crystal Waters Chlorine Free Chemical System and the Nature 2 Chemical System.</p>

				</div>

			</div>

		</div>

	</article>


<?php include 'footer.php'; ?>