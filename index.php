<?php include 'header.php'; ?>

	<article class="content">

		<div class="main-banner">
			<div class="arrows" id="white-arrows">
				<div class="left-arrow">
					<i class="fa fa-angle-left" aria-hidden="true"></i>
				</div>
				<div class="right-arrow">
					<i class="fa fa-angle-right" aria-hidden="true"></i>
				</div>
			</div>
			<div class="banner-feature">
				<h3 class="feature">MASSIVE SALE ON ALL <br>NOOSA ENTERTAINER SPAS</h4>
				<p class="secondary-headline">TAKE AN ADDITIONAL 50% OFF</p>
				<div class="button"><a href="products.php" class="button-link hvr-sweep-to-right">VIEW PRODUCTS</a></div>
			</div>
		</div>

		<div class="wrapper">
			<div class="product-box-container col-3" id="home-feature">

				<a href="productrange.php" class="product-box">
					<div class="overlay">
						<p>VIEW RANGE</p>
					</div>
					<div class="product-name">
						<h3>SPA RANGE</h3>
					</div>
					<img src="assets/images/product-image.jpg"/>
				</a>

				<a href="productrange.php" class="product-box">
					<div class="overlay">
						<p>VIEW RANGE</p>
					</div>
					<div class="product-name">
						<h3>SWIM SPA RANGE</h3>
					</div>
					<img src="assets/images/product-image.jpg"/>
				</a>

				<a href="productrange.php" class="product-box">
					<div class="overlay">
						<p>VIEW RANGE</p>
					</div>
					<div class="product-name">
						<h3>SPA BATH RANGE</h3>
					</div>
					<img src="assets/images/product-image.jpg"/>
				</a>

			</div>
		</div>


		<div class="about-section">
			<div class="wrapper">
				<h2>LOOKING FOR A SPA, SWIM SPA OR PLUNGE POOL WITH A DIFFERENCE?</h2>
				<p class="secondary-headline">THEN SPA-RITE IS THE RIGHT PLACE FOR YOU</p>
				<div class="about-content-container">
					<div class="video">
						<iframe src="https://www.youtube.com/embed/vq5zdfJOlrw" frameborder="0" allowfullscreen></iframe>
					</div>
					<div class="about-content">
						<p>Spa-Rite is a family run business that has been operating for 12 years, and specialises in spas with power saving technology to give you the lowest possible running cost.</p>

						<p>Spa-Rite is based in Melbourne in the Eastern suburbs, and has many years of experience and local knowledge of the spa industry. Spa-Rite is an Oasis Spa and Coast Spa distributor for Victoria and Tasmania, with a retail store in Bayswater Melbourne.</p>

						<div class="button-pink"><a href="contact.php" class="button-pink-link">VISIT OUR SHOWROOM</a></div>

					</div>
				</div>
			</div>
		</div>


		<div class="testimonials">
			<div class="arrows" id="white-arrows">
				<div class="left-arrow">
					<i class="fa fa-angle-left" aria-hidden="true"></i>
				</div>
				<div class="right-arrow">
					<i class="fa fa-angle-right" aria-hidden="true"></i>
				</div>
			</div>
			<div class="wrapper">
				<p class="testimonial">“No problem is too big or small for the Spa-Rite team to handle”</p>
				<p class="author">JOHN SMITH</p>
			</div>
		</div>

		<div class="photo-gallery">
			<div class="wrapper">
				<h2>SPA-RITE PHOTO GALLERY</h2>
				<p class="secondary-headline">VIEW ALL GALLERIES</p>
				<div class="photo-box-container">
					<a href="#" class="photo-box">
						<div class="overlay">
							<p>PHOTO ALBUM NAME</p>
						</div>
					</a>
					<a href="#" class="photo-box">
						<div class="overlay">
							<p>PHOTO ALBUM NAME</p>
						</div>
					</a>
					<a href="#" class="photo-box">
						<div class="overlay">
							<p>PHOTO ALBUM NAME</p>
						</div>
					</a>
					<a href="#" class="photo-box">
						<div class="overlay">
							<p>PHOTO ALBUM NAME</p>
						</div>
					</a>
					<a href="#" class="photo-box desktop-hidden">
						<div class="overlay">
							<p>PHOTO ALBUM NAME</p>
						</div>
					</a>

				</div>
			</div>
		</div>

	</article>

<?php include 'footer.php'; ?>
