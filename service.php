<?php include 'header.php'; ?>

	<article class="content">
		<div class="page-banner">
			<div class="page-banner-headline">
				<h3 class="feature">SPARITE SERVICES</h3>
				<p class="secondary-headline">BOOK YOUR SERVICING OR REPAIRS WITH US</p>
			</div>	
		</div>

		<div class="wrapper">

		<ul class="tab-navigation">
				<li class="active" rel="tab1">SPARITE SERVICES</li>
				<li rel="tab2">SPA REPAIRS</li>
			</ul>

			<div class="tab_container">
				<p class="d_active tab_drawer_heading" rel="tab1">SPA SERVING </p>
			  	<div id="tab1" class="tab_content">
			  		
					<h2>SPARITE SPA SERVICING</h2>

					<div class="col-2">
						<p>Here at Spa-Rite we believe in giving the best customer service possible. From the beginning of first meeting our customers, and even after delivery, keeping a good relationship with our customers.</p>

						<p>We offer an after sales service and a we carry a full range of spare parts for many brands of spas. So even if you don’t have a Spa-Rite spa our service is still available.</p>
					</div>
						
					<div class="col-2">
						<p>Spa-Rite can assist with the following:</p>

						<ul>
							<li>Touchpads</li>
							<li>Controllers</li>
							<li>Service Calls</li>
							<li>Jet Replacements</li>
							<li>Replacement Ozone Units</li>
							<li>Spa and Swim Spa Repairs</li>
							<li>Hardcovers</li>
							<li>Chemical Assistance</li>
							<li>In ground Installs</li>
							<li>Acrylic Repairs</li>
							<li>Pump Replacements</li>
							<li>Spare Part Replacements</li>
							<li>Spa and swim spa leaks</li>
						</ul>
					</div>
			  	</div>
				  <!-- #tab1 -->
				<p class="tab_drawer_heading" rel="tab2">SPA REPAIRS </p>
			 	<div id="tab2" class="tab_content">
			  		<h2>SPA REPAIRS MELBOURNE</h2>

					<div class="col-2">
						<p>At Spa-Rite we are able to carry out service and repairs to all makes and models of spas and swim spas.</p>

						<p>Our service technicians receive a constant flow of technical information from the manufactures to ensure they maintain the highest possible standard of competence.</p>

						<p>Not only can we carry out service and repairs, we have a large stock of spare parts for your unit.</p>
					</div>
						
					<div class="col-2">
						<p>We can carry out the following service and repairs to:</p>

						<ul>
							<li>Spa/Swim Spa service</li>
							<li>Jet replacements</li>
							<li>Key pads</li>
							<li>Controllers</li>
							<li>Pump repairs or new units</li>
							<li>Heat pump sales</li>
							<li>Filter sales</li>
							<li>Chemical sales</li>
							<li>Water testing</li>
							<li>Many other service and repairs</li>
						</ul>
					</div>
			  	</div>
			</div>
		</div>

	<div class="highlight-section">
		<div class="wrapper">
			<h2>BOOK YOUR SERVICING IN NOW</h2>
			<p class="secondary-headline">OR CALL US DIRECTLY ON (03) 9720 4461</p>
			<form class="wet-test">
				 <input type="text" placeholder="FIRST NAME"><br>
				 <input type="text" placeholder="LAST NAME"><br>
				 <input type="text" placeholder="EMAIL"><br>
				 <input type="text" placeholder="PHONE NUMBER"><br>
				 <select name="service-type">
				 	<option value="false">-- PLEASE SELECT A SERVICE TYPE --</option>
					<option>General service</option>
					<option>Controller not working</option>
					<option>Pump not working</option>
					<option>Heater not working</option>
					<option>Jets not working</option>
					<option>Spa tripping</option>
					<option>Spa leaking</option>
					<option>Need new hard cover</option>
					<option>Other</option>
				</select>
				<select name="spa-brand">
				 	<option value="false">-- SELECT A SPA BRAND  -</option>
					<option>Oasis Spa</option>
					<option>Coast Spa</option>
					<option>Other</option>
				</select>
				<select name="spa-age">
				 	<option value="false">-- PLEASE SELECT AGE OF SPA --</option>
					<option>Less Than 1 Year</option>
					<option>1-2 Years</option>
					<option>2-5 Years</option>
					<option>5-10 Years</option>
					<option >10+ Years</option>
				</select>
				 <input type="text" placeholder="SPA MODEL (IF KNOWN)"><br>
				 <input type="text" placeholder="CONTROLLER TYPE (IF APPLICAPLE)"><br>
				 <textarea placeholder="YOUR SERVICE REQUEST DETAILS" rows="7"></textarea>
				 <div class="button"><a href="#" class="button-link hvr-sweep-to-right">SUBMIT SERVICE FORM</a></div>
			</form>
		</div>
	</div>

	</article>
	
<?php include 'footer.php'; ?>