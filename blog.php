<?php include 'header.php'; ?>

	<article class="content">

		<div class="page-banner">
			<div class="page-banner-headline">
				<h3 class="feature">FROM OUR BLOG</h3>
				<p class="secondary-headline">KEEP UP TO DATE WITH THE LATEST SPA TIPS </p>
			</div>	
		</div>

		<div class="wrapper">

			<div class="posts">
				<div class="post-box-container col-2">

					<a href="blog-single.php" class="post-box">
						<div class="overlay">
							<p>VIEW</p>
						</div>
						<div class="post-name">
							<h3>POOL COMPLIANT STEPS</h3>
							<p class="blog-details">BY: DANIELLE
							<span class="right">MAY 14, 2015</span></p>
						</div>
						<img src="assets/images/blog-sample.jpg"/>
					</a>
					<a href="blog-single.php" class="post-box">
						<div class="overlay">
							<p>VIEW</p>
						</div>
						<div class="post-name">
							<h3>PORTABLE VS IN GROUND SPAS</h3>
							<p class="blog-details">BY: DANIELLE
							<span class="right">MAY 14, 2015</span></p>
						</div>
						<img src="assets/images/blog-sample.jpg"/>
					</a>
					<a href="blog-single.php" class="post-box">
						<div class="overlay">
							<p>VIEW</p>
						</div>
						<div class="post-name">
							<h3>POOL COMPLIANT STEPS</h3>
							<p class="blog-details">BY: DANIELLE
							<span class="right">MAY 14, 2015</span></p>
						</div>
						<img src="assets/images/blog-sample.jpg"/>
					</a>
					<a href="blog-single.php" class="post-box">
						<div class="overlay">
							<p>VIEW</p>
						</div>
						<div class="post-name">
							<h3>WHAT TO LOOK AT BEFORE PURCHASING A SPA</h3>
							<p class="blog-details">BY: DANIELLE
							<span class="right">MAY 14, 2015</span></p>
						</div>
						<img src="assets/images/blog-sample.jpg"/>
					</a>
					<a href="blog-single.php" class="post-box">
						<div class="overlay">
							<p>VIEW</p>
						</div>
						<div class="post-name">
							<h3>OASIS SPAS: THE PERFECT SPA MASSAGE</h3>
							<p class="blog-details">BY: DANIELLE
							<span class="right">MAY 14, 2015</span></p>
						</div>
						<img src="assets/images/blog-sample.jpg"/>
					</a>
					<a href="blog-single.php" class="post-box">
						<div class="overlay">
							<p>VIEW</p>
						</div>
						<div class="post-name">
							<h3>OASIS SPAS LIFETIME SUPPORT</h3>
							<p class="blog-details">BY: DANIELLE
							<span class="right">MAY 14, 2015</span></p>
						</div>
						<img src="assets/images/blog-sample.jpg"/>
					</a>

					<a class="post-box" id="empty"></a>
					<a class="post-box" id="empty"></a>
				</div>
			</div>
		</div>


	</article>


<?php include 'footer.php'; ?>