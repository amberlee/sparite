		<footer class="main-footer">
			<div class="footer-content wrapper">

				<div class="footer-menu">
					<div class="footer-links">
						<p>SUPPORT</p>
						<ul>
							<li><a href="#">Installation Guides</a></li>
							<li><a href="#">Permits</a></li>
							<li><a href="#">FAQ</a></li>
							<li><a href="#">Owner Manuals</a></li>
						</ul>
					</div>
					<div class="footer-links" id="showroom">
						<p>VISIT OUR SHOWROOM</p>
						<ul>
							<li>Spa-Rite - Shop 3</li>
							<li>Cnr Bayfield Rd East and Canterbury Rd</li>
							<li>Bayswater North Vic 3152</li>
							<li>Melways May: 50 J11</li>
						</ul>
					</div>
					<div class="footer-links">
						<p>CONTACT</p>
						<ul>
							<li>Call: (03) 9720 4461</li>
							<li>Fax: (03) 9720 4481</li>
						</ul>
					</div>
					<div class="footer-links">
						<p>HOURS</p>
						<ul>
							<li>Mon-Sat:  9am - 5pm</li>
							<li>Sunday:  10am - 5pm</li>
						</ul>
					</div>
				</div>


				<div class="google-maps">
					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3151.413307733464!2d145.2801123156242!3d-37.827208979749926!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6ad63b1d5c5ea37b%3A0x8b29652bf4103143!2sSpa-Rite!5e0!3m2!1sen!2sca!4v1465577749879" width="500" height="250" frameborder="0" style="border:0" allowfullscreen></iframe>
				</div>
			</div>

				<div class="footer-copyright">
					<div class="wrapper">
						<p>SPA-RITE &copy; 2015</p>
						<a href="mailto:danielle@sparite.com.au" target="_top"><i class="fa fa-envelope" aria-hidden="true"></i></a>
						<a href="https://www.facebook.com/sparitespas" target="_blank"><i class="fa fa-facebook-official" aria-hidden="true"></i></a>
					</div>
				</div>

		</footer>

	</div>
</body>
</html>
