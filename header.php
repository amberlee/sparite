<!DOCTYPE html>
<html lang="en">
<head>
	<title>Spas, Swim Spas and More | SpaRite Melbourne</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- Just shove everything in here for now. We will address for performance when integrating into WP-->
	<link rel="stylesheet" href="assets/css/global.min.css">
	<link rel="stylesheet" href="assets/css/font-awesome/font-awesome.css">
	<link href='https://fonts.googleapis.com/css?family=Roboto:100,300,400|Oswald:400,700' rel='stylesheet' type='text/css'>
	<!-- <link href="assets/css/hover.css" rel="stylesheet" media="all"> -->

	<script src="assets/js/vendor/jquery-1.11.3.min.js"></script>
	<script src="assets/js/src/app.js"></script>

	<!-- Handy for dev, auto reloads page, remove if you dont like-->
	<script src="http://livejs.com/live.js"></script>
</head>
<body>

	<div class="full-wrapper">

		<header class="main-header">

			<div class="logo-container">
				<div class="wrapper">
					<a href="index.php" class="logo"><img src="assets/images/logo.png" alt="SpaRite Logo"></a>
				</div>
			</div>

			<div class="social-nav">
				<div class="wrapper">
					<ul>
						<li><a href="/buildyourownspa.php">BUILD YOUR OWN SPA</a>  |  </li>
						<li><a href="/blog.php"><i class="fa fa-rss-square" aria-hidden="true"></i></a></li>
						<li><a href="https://www.facebook.com/sparitespas/" target="_blank"><i class="fa fa-facebook-official" aria-hidden="true"></i></a></li>
						<li><a href="#"><i class="fa fa-search" aria-hidden="true"></i></a></li>
					</ul>
				</div>
			</div>

			<div class="relative-nav">
				<nav class="main-nav">
					<div class="wrapper">
						<div class="phone-number">
							<a href="tel:+61397204461" class="mobile-phone"><img src="assets/images/mobile-phone.png" alt="Phone Icon"></a>
							<h4><img src="assets/images/phone-icon.png" alt="Phone Icon"> (03) 9720 4461 <h4>
						</div>
						<ul>
							<li class="menu-item-has-children"><a href="products.php">PRODUCTS</a>
								<ul class="sub-menu">
									<li><a href="productrange.php">SPAS</a></li>
									<li><a href="productrange.php">SWIM SPAS</a></li>
									<li><a href="productrange.php">IN GROUND SPAS</a></li>
									<li><a href="productrange.php">SPA BATHS</a></li>
									<li><a href="productrange.php">ACCESSORIES</a></li>
									<li><a href="productrange.php">GAZEBOS</a></li>
									<li><a href="productrange.php">EQUIPMENT</a></li>
								</ul>
							</li>
							<li><a href="productrange.php">SPARE PARTS </a></li>
							<li><a href="maintence-guides.php">MAINTENCE GUIDES</a></li>
							<li><a href="service.php">SERVICE</a></li>
							<li class="menu-item-has-children"><a href="#">SUPPORT</a>
								<ul class="sub-menu">
									<li><a href="#">INSTALLATION GUIDE</a></li>
									<li><a href="permits.php">PERMITS</a></li>
									<li><a href="faq.php">FAQ</a></li>
									<li><a href="ownermanuals.php">OWNER MANUALS</a></li>
									<li><a href="#">SPA BUYERS GUIDE</a></li>
								</ul>
							</li>
							<li><a href="contact.php">CONTACT</a></li>
							<li class="specials"><a href="productrange.php">SPECIALS</a></li>
						</ul>
					</div>
				</nav>
			</div>

		</header>

		<!-- Mobile Navigation -->

		<a href="#mobile-nav" id="mobile-nav-toggle" class="mobile-nav-toggle"></a>
			<nav class="mobile-nav" id="mobile-nav">
				<ul>
					<li class="current"><a href="index.php">HOME</a></li>
						<li class="menu-item-has-children"><a href="#">PRODUCTS</a>
							<ul class="sub-menu">
								<li><a href="productrange.php">SPAS</a></li>
								<li><a href="productrange.php">SWIM SPAS</a></li>
								<li><a href="productrange.php">IN GROUND SPAS</a></li>
								<li><a href="productrange.php">SPA BATHS</a></li>
								<li><a href="productrange.php">ACCESSORIES</a></li>
								<li><a href="productrange.php">GAZEBOS</a></li>
								<li><a href="productrange.php">EQUIPMENT</a></li>
							</ul>
						</li>
						<li><a href="productrange.php">SPARE PARTS </a></li>
						<li><a href="maintence-guides.php">MAINTENCE GUIDES</a></li>
						<li><a href="service.php">SERVICE</a></li>
						<li class="menu-item-has-children"><a href="#">SUPPORT</a>
							<ul class="sub-menu">
								<li><a href="#">INSTALLATION GUIDE</a></li>
								<li><a href="permits.php">PERMITS</a></li>
								<li><a href="faq.php">FAQ</a></li>
								<li><a href="ownermanuals.php">OWNER MANUALS</a></li>
								<li><a href="#">SPA BUYERS GUIDE</a></li>
							</ul>
						</li>
						<li><a href="buildyourownspa.php">BUILD YOUR OWN SPA</a></li>
						<li><a href="blog.php">BLOG</a></li>
						<li><a href="contact.php">CONTACT</a></li>
						<li class="specials"><a href="productrange.php">SPECIALS</a></li>
					</ul>

					<div class="phone-number-mobile">
						<h4><img src="assets/images/phone-icon.png" alt="Phone Icon"> (03) 9720 4461 <h4>
					</div>
			</nav>
