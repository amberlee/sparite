<?php include 'header.php'; ?>

	<article class="content">

		<div class="page-banner-blog">
			<div class="page-banner-headline">
				<h3 class="feature">POOL COMPLIANT STEPS</h3>
				<p class="secondary-headline">WRITTING BY: DANIELLE ON MAY 14, 2015</p>
			</div>	
		</div>

		<div class="wrapper">

			<div class="headline-left">
				<h2>DON'T WANT TO FENCE OFF YOUR ABOVE GROUND SWIM SPA?</h2>
				<div class="text">
					<p>Spa-Rite has recently patented a set of pool complaint steps. Our Pool Compliant Steps are completely maintenance free and fenced off. Eliminating the requirement of needing a safety barrier going around your spa, providing the rest of your spa has no foot holds around your spa at 1200mm high. Even better, our Pool Compliant steps are made out of Thermo-wood meaning they will match the colour of your spa cabinet.</p>

					<p>Step Safe Pool Compliant Steps are available in Lucabond or in slate or mocha Thermo-wood cladding</p>

					<p><strong>Length:</strong> 910mm<br>
					<strong>Width:</strong> 910mm<br>
					<strong>Height:</strong> 1245mm<br></p>

				</div>

			</div>

			<div class="pagination">
				<div class="text-left">
				
				<h3><a href="#"><i class="fa fa-long-arrow-left" aria-hidden="true"></i>  PREVIOUS</h3><p>PORTABLE VS IN GROUND SPA</a></p>
				</div>

				<div class="text-right">
				<h3><a href="#">NEXT  <i class="fa fa-long-arrow-right" aria-hidden="true"></i></h3><p>SWIM SPAS FROM SPARITE </a></p>
				</div>
			</div>

		</div>

	</article>


<?php include 'footer.php'; ?>