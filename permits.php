<?php include 'header.php'; ?>

	<article class="content">

		<div class="page-banner">
			<div class="page-banner-headline">
				<h3 class="feature">THE TRUTH ABOUT PERMITS</h3>
				<p class="secondary-headline">ALL SPAS AND SWIM SPAS IN VICTORIA REQUIRE SAFETY BARRIERS AND PERMITS</p>
			</div>	
		</div>

		<div class="wrapper">

			<div class="headline-left">
				<h2>PERMIT FACTS</h2>
				<div class="text">
					<p>My name is Jon Neilsen and I own Spa-Rite Spas situated in Bayswater North, Melbourne Victoria. As many would be aware, the industry has just completed the spa show in early February. It was noticed during this show that many people were not aware that if they purchase a spa then they would legally require a permit and a safety barrier.</p>

					<p>The Swimming Pool and Spa Association (SPASA) clearly state “Pool and spa fencing is required by legislation for any swimming pool or spa in excess of 300mm in depth and must be maintained for the life of the pool or spa. The key elements of the relevant Australian Standard, A.S. 1926.1-2007 are designed to deny access by unsupervised young children, especially those under 5 years of age, to the swimming pool area. <br><a href="http://www.spasavic.com.au/docs/Factsheet2.pdf">http://www.spasavic.com.au/docs/Factsheet2.pdf</a></p>

					<blockquote><p>All spas and swim spas in Victoria require safety barriers and permits.</p></blockquote>

					<p>My customer care program, in conjunction with an external contractor, provides the arrangement of permits, and offers the correct advice on safety barriers on site. For information on what Spa-Rite can do to help you get ready for your spa, click on the <a href="#">Installation Guide</a> link or call us on 9720 4461.</p>

					<p>Manufactures worldwide would regard Spa-Rite as leaders and innovators for the spa industry. We at Spa-Rite offer thicker acrylic shells, smarter controllers, more insulation and lower running costs. We do not sell the cheapest spas, and maybe there is no market for cheap spas, as we specialize with lower running costs keeping in mind that power is getting more and more expensive in Victoria. I’m humble and proud that Spa-Rite here in Melbourne Australia, won new International Dealer of the Year for 2013  Coast Spas Canada. It is quite a family achievement. Coast Spas has now just won people’s choice for the best built spa in the world for 2013. It does not get any better than that, and for us to represent this company, well what can I say.</p>

					<p>Again, we have been chosen to be distributor of Oasis Spas here in Victoria and Tasmania, and with Oasis now being distributed to over 35 countries around the world, and leading Europe in technology and energy efficiency, this is quite an achievement. Germany and many other countries in Europe, including Australia, have joined forces with the designers at Oasis Spas to build one of the strongest, most reliable and comfortable spas available anywhere in the world.</p>

					<p>Many that read this article will be amazed at the fact that for a swim spa that is over 1200 high, they still do require a safety barrier and a permit. Any swim spa, spa or body of water more than 300 mm deep requires a safety barrier and a permit for the safety barrier. I have spoken with many families that say we are buying the swim spa and as it is tall we can have removable steps. It is for that reason I have taken the initiative to design and patent a set of steps that are pool complaint. Step Safe can be used on any brand of swim spa and have full safety and patent approvals. You cannot have removable steps, they do not comply.</p>

					<p>For more information regarding permits and safety barriers click on the below links:</p>

					<p><a href="http://www.buildingcommission.com.au/consumers/swimming-pool-and-spa-safety-barriers">http://www.buildingcommission.com.au/consumers/swimming-pool-and-spa-safety-barriers</a></p>

					<p><a href="http://www.spasavic.com.au/docs/Factsheet2.pdf">http://www.spasavic.com.au/docs/Factsheet2.pdf</a></p>
				</div>

			</div>

		</div>

	</article>


<?php include 'footer.php'; ?>